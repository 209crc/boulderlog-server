module Api
  class Api::ArticleController < ApplicationController
    def index
        @article = Article.where("title = ?", params[:title])
        render json: @article
    end
  end
end
