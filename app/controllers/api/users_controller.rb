module Api
  class Api::UsersController < ApplicationController
    # todo : 権限チェック
    protect_from_forgery :except => [:login]

    def login
      unless (params[:name])
        render json: {ok:false}.to_json
        return
      end

      if ((params[:fId] && params[:tId]) ||
          (params[:tId] && params[:lId]) ||
          (params[:lId] && params[:fId]))
          render json: {ok:false}.to_json
          return
      end

      if (params[:fId])
        user = User.find_or_initialize_by(fId: params[:fId])
      elsif (params[:tId])
        user = User.find_or_initialize_by(tId: params[:tId])
      elsif (params[:lId])
        user = User.find_or_initialize_by(lId: params[:lId])
      end

      # todo : ログインIDを偽装されていたらセキュリティ的にダメ?
      if user.new_record?
        user.name = params[:name]
        if (params[:email])
          user.email = params[:email]
        end  
        user.save!
        @loggedinUser = user
      else
        @loggedinUser = user
      end
    end
  end
end
