module Api
  class Api::SpotController < ApplicationController
    protect_from_forgery :except => [:provisionallyRegister]

    def provisionallyRegister
      unless (params[:name] && params[:prefectureId])
        raise 'invalid parameter'
      end

      spot = Spot.find_or_initialize_by(name: params[:name], prefecture_id: params[:prefectureId])
      if spot.new_record?
        spot.save!
      else
        raise 'already provisionally registered'
      end

      render json: {ok:true}.to_json
    end
  end
end
