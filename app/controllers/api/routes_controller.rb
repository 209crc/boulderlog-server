module Api
  class Api::RoutesController < ApplicationController
    protect_from_forgery :except => [:create]

    def index
        @routes = Route.where(spot_id: params[:spot_id])

        # todo jbulderにする
        render json: @routes
    end

    def create
      # todo : 権限チェック
      unless (params[:spot_id] && params[:user_id] && params[:name] && params[:grade])
        render json: {ok:false}.to_json
        return
      end

      route = Route.create(spot_id: params[:spot_id], user_id: params[:user_id], name: params[:name], grade: params[:grade])
      route.save!
      
      render json: {ok:true}.to_json

    end
  end
end
