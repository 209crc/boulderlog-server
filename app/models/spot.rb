class Spot < ApplicationRecord
  has_many :routes

  validates :name, presence: true
  validates :prefecture_id, presence: true

  # todo ; 都道府県コードの範囲をvalidate

end
