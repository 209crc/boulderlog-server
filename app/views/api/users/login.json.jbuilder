json.set! :ok, true

if @loggedinUser.id != 0
    json.set! :user do
        json.set! :id, @loggedinUser.id
        json.set! :name, @loggedinUser.name

        if !@loggedinUser.fId.nil?
            json.set! :fId, @loggedinUser.fId
        elsif !@loggedinUser.tId.nil?
            json.set! :tId, @loggedinUser.tId
        elsif !@loggedinUser.lId.nil?
            json.set! :lId, @loggedinUser.lId
        end
    end
end
