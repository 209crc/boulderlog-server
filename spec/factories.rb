# config: utf-8
FactoryGirl.define do
  factory :user do
    name "まかじき"
  end

  factory :userWithEmail do
    # todo : userを使う形にする
    name "まかじき"
    email "a@a.com"
  end

  factory :spot do
    name "MyString"
    prefecture_id 1
    address "MyString"
    authorized false
  end

  factory :route do
    association :user
    association :spot

    name "重力と呼吸"
  end

end