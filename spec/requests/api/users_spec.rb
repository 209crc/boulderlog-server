# config: utf-8
require 'rails_helper'
RSpec.describe "ユーザ関連のAPI", type: :request do
  describe "POST /api/users" do
    context({種別: "正常", 要因: {ユーザ情報: "user", パラメータ: "name, fId"}}) do
      before do

        # todo : 権限チェック
        post "/api/users/login", :params => {name: "まかじき", fId: "testtest", email: "a@a.com"}

        @json = JSON.parse(response.body)
      end

      it {expect(@json['ok']).to be true}
      it {expect(User.find_by(fId: "testtest").name).to eq "まかじき"}
      it {expect(User.find_by(fId: "testtest").email).to eq "a@a.com"}
    end

    context({種別: "正常", 要因: {ユーザ情報: "user", パラメータ: "name, tId"}}) do
      before do

        # todo : 権限チェック
        post "/api/users/login", :params => {name: "まかじき", tId: "testtest"}

        @json = JSON.parse(response.body)
      end

      it {expect(@json['ok']).to be true}
      it {expect(User.find_by(tId: "testtest").name).to eq "まかじき"}
    end

    context({種別: "正常", 要因: {ユーザ情報: "user", パラメータ: "name, lId"}}) do
      before do

        # todo : 権限チェック
        post "/api/users/login", :params => {name: "まかじき", lId: "testtest"}

        @json = JSON.parse(response.body)
      end

      it {expect(@json['ok']).to be true}
      it {expect(User.find_by(lId: "testtest").name).to eq "まかじき"}
    end

    context({種別: "正常", 要因: {ユーザ情報: "user", パラメータ: "nameあり, fId, tId"}}) do
      before do

        # todo : 権限チェック
        post "/api/users/login", :params => {name: "まかじき", fId: "ftesttest", tId: "ttesttest"}

        @json = JSON.parse(response.body)
      end

      it {expect(@json['ok']).to be false}
    end

    context({種別: "正常", 要因: {ユーザ情報: "user", パラメータ: "nameあり, tId, lId"}}) do
      before do

        # todo : 権限チェック
        post "/api/users/login", :params => {name: "まかじき", tId: "ttesttest", lId: "ltesttest"}

        @json = JSON.parse(response.body)
      end

      it {expect(@json['ok']).to be false}
    end

    context({種別: "正常", 要因: {ユーザ情報: "user", パラメータ: "name変更 email変更, fId"}}) do
      before do

        # todo : 権限チェック
        post "/api/users/login", :params => {name: "まかじき", lId: "ltesttest", fId: "ftesttest"}

        @json = JSON.parse(response.body)
      end

      it {expect(@json['ok']).to be false}
    end

    context({種別: "正常", 要因: {ユーザ情報: "user", パラメータ: "登録済み"}}) do
      before do

        # todo : 権限チェック
        post "/api/users/login", :params => {name: "まかじき", fId: "ftesttest", email: "a@a.com"}

        @json1 = JSON.parse(response.body)

        post "/api/users/login", :params => {name: "まかじき2", fId: "ftesttest", email: "b@b.com"}
        @json2 = JSON.parse(response.body)
      end

      it {expect(@json1['ok']).to be true}
      it {expect(@json2['ok']).to be true}
      it {expect(User.find_by(fId: "ftesttest").name).to eq "まかじき"}
      it {expect(User.find_by(fId: "ftesttest").email).to eq "a@a.com"}
      it {expect(User.where(fId: "ftesttest").count).to eq 1}

    end
  end
end