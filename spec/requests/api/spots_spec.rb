# config: utf-8
require 'rails_helper'
# todo ファイル名はspot_spec?
RSpec.describe "スポット関連のAPI", type: :request do
  describe "POST /api/spot/provisionallyRegister" do
    context({種別: "正常", 要因: {ユーザ情報: "user", }}) do
      before do
        @spot = FactoryGirl.create(:spot)
        @id = @spot.id
        @spot.name = "テストジム"
        
        # todo : 権限チェック
        post "/api/spot/provisionallyRegister", :params => {:name => @spot.name, :prefectureId => @spot.prefecture_id}

        @json = JSON.parse(response.body)
      end

      it {expect(@json['ok']).to be true}
      it {expect(Spot.find(@id).authorized).to be false}
    end
  end
end