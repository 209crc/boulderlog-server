# config: utf-8
require 'rails_helper'
RSpec.describe "ルート関連のAPI", type: :request do
  describe "POST /api/routes" do
    context({種別: "正常", 要因: {ユーザ情報: "user", パラメータ: "全部あり"}}) do
      before do

        @spot = FactoryGirl.create(:spot)
        @user = FactoryGirl.create(:user)

        # todo : 権限チェック
        post "/api/routes/create", :params => {spot_id: @spot.id, user_id: @user.id, name: "YourSong", grade: 50}

        @json = JSON.parse(response.body)
      end

      it {expect(@json['ok']).to be true}
      it {expect(Route.find_by(spot_id: @spot.id, user_id: @user.id).name).to eq "YourSong"}
      it {expect(Route.find_by(spot_id: @spot.id, user_id: @user.id).grade).to be 50}
    end

    context({種別: "正常", 要因: {ユーザ情報: "user", パラメータ: "nameなし"}}) do
      before do

        @spot = FactoryGirl.create(:spot)
        @user = FactoryGirl.create(:user)

        # todo : 権限チェック
        post "/api/routes/create", :params => {spot_id: @spot.id, user_id: @user.id, grade: 50}

        @json = JSON.parse(response.body)
      end

      it {expect(@json['ok']).to be false}
    end

  end
end