Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api, {format: 'json'} do
    namespace :article do
      get "/" , :action => "index"
    end

    # 'users'
    namespace :users do
      post "/login", :action => "login"
    end

    # 'spot'
    # spotsと一緒の方が正しい?
    namespace :spot do
      post "/provisionallyRegister", :action => "provisionallyRegister"
    end

    # 'spots'
    namespace :spots do
      get "/" , :action => "index"
    end

    # 'routes'
    namespace :routes do
      # todo actionはindexでいい?
      get "/:spot_id" , :action => "index"
      post "/create" , :action => "create"
    end

  end
end
