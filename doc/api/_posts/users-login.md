---
category: ユーザ
path: 'api/users/login'
type: 'POST'
---

ユーザを作成する

# Request
```{
    fId: "asdfgh",     // 形式はよく分からない。どれか必須
    tId: "asdfgh",     // 形式はよく分からない。どれか必須
    lId: "asdfgh",     // 形式はよく分からない。どれか必須
    name: "まかじき",   // 必須
    email: "a@a.com",  // あれば
}```

# Response
"成功した場合",

```Status OK```
``` {
    ok: true
    user: {
        id: 123,
        fId: "asdfgh",     // 形式はよく分からない。どれかだけ
        tId: "asdfgh",     // 形式はよく分からない。どれかだけ
        lId: "asdfgh",     // 形式はよく分からない。どれかだけ
        name: "まかじき",
        // emailは必要ないと思うので、送らない
    }
}```
