---
category: スポット
path: 'api/routes/create'
type: 'POST'
---

ルートを新規投稿する

# Request
```{
    spot_id: 123,
    user_id: 12345,
    name: "重力と呼吸",
    grade: 50, <!-- https://docs.google.com/spreadsheets/d/1zb7W9-o19XdxrBv0VXquVYuvIJwJFmCDmpoOinYPhAs/edit#gid=0 -->
    <!-- todo : image_urlはまだない -->
    <!-- todo : パラメータには画像を渡す? -->
    <!-- todo : サンプルURLを変える -->
    image_url_1: "http://www.nikon-image.com/products/slr/lineup/d5300/img/sample/pic_03_l.jpg",
    image_url_2: "http://www.nikon-image.com/products/slr/lineup/d5300/img/sample/pic_03_l.jpg",
    image_url_3: "http://www.nikon-image.com/products/slr/lineup/d5300/img/sample/pic_03_l.jpg",
    image_url_4: "http://www.nikon-image.com/products/slr/lineup/d5300/img/sample/pic_03_l.jpg",
    image_url_5: "http://www.nikon-image.com/products/slr/lineup/d5300/img/sample/pic_03_l.jpg",

}```

# Response
"成功した場合",

```Status OK```
``` {
    ok: true
}```
