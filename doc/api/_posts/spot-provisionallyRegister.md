---
category: スポット
path: 'api/spot/provisionallyRegister'
type: 'POST'
---

スポットを仮登録する

# Request
```{
    name: "空の向こうへ四足歩行",
    prefectureId: 13
}```

# Response
"成功した場合",

```Status OK```
``` {
    ok: true
}```

todo : 他のAPIについても書く
