class SetDefaultSpot < ActiveRecord::Migration[5.1]
  def up
    change_column :spots, :name, :string, null: false
    change_column :spots, :prefecture_id, :integer, null: false
    change_column :spots, :authorized, :boolean, default: false
  end

  def down
    change_column :spots, :name
    change_column :spots, :prefecture_id
    change_column :spots, :authorized
  end

end
