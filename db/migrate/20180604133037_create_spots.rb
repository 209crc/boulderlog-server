class CreateSpots < ActiveRecord::Migration[5.1]
  def change
    create_table :spots do |t|
      t.string :name
      t.integer :prefecture_id
      t.string :address
      t.boolean :authorized

      t.timestamps
    end
  end
end
