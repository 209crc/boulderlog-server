class AddEmailUsers < ActiveRecord::Migration[5.1]
  def change
     add_column :users, :email, :string, index: true, :after => :name
  end
end
