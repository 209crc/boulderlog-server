class AddSnsIdUsers < ActiveRecord::Migration[5.1]
  def change
     add_column :users, :fId, :string, index: true, :after => :name
     add_column :users, :tId, :string, index: true, :after => :fId
     add_column :users, :lId, :string, index: true, :after => :tId
  end
end
