class CreateRoutes < ActiveRecord::Migration[5.1]
  def change
    create_table :routes do |t|
      t.integer :spot_id, null: false, foreign_key: true
      t.integer :user_id, null: false, foreign_key: true
      t.string :name, null: false
      t.integer :grade
      t.string :image_url1
      t.string :image_url2
      t.string :image_url3
      t.string :image_url4
      t.string :image_url5
      t.timestamps
      t.datetime :removed_at

      t.belongs_to :spot
    end
  end
end
